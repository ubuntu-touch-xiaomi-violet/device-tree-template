Files located in this directory will be included in the system image and will
be overlaid on top of the root filesystem at runtime. This happens via
bind-mounts created by the initramfs at boot.

For example, here's the overlay tree from a Xiaomi device:

```
├── overlay
│   └── system
│       ├── etc
│       │   ├── init
│       │   │   ├── bluebinder.conf
│       │   │   ├── mount-android.conf
│       │   │   └── mtp-state.conf
│       │   ├── ofono
│       │   │   ├── main.conf
│       │   │   └── ril_subscription.conf
│       │   └── ubuntu-touch-session.d
│       │       └── android.conf
│       ├── lib
│       │   └── udev
│       │       └── rules.d
│       │           └── 70-lavender.rules
│       └── usr
│           └── share
│               ├── upstart
│               │   └── sessions
│               │       └── mtp-server.conf
│               └── usbinit
│                   └── setupusb
```
